﻿using StackExchange.Redis;
using StudioKit.ErrorHandling.Interfaces;
using System;

namespace StudioKit.Caching.Clients
{
	public class RedisConnectionFactory : IDisposable
	{
		private ConnectionMultiplexer _connection;
		private readonly string _connectionString;
		private readonly IErrorHandler _errorHandler;

		public RedisConnectionFactory(string connectionString, IErrorHandler errorHandler)
		{
			_connectionString = connectionString;
			_errorHandler = errorHandler;
		}

		public ConnectionMultiplexer Connection
		{
			get
			{
				if (_connection != null)
					return _connection;

				try
				{
					_connection =
						ConnectionMultiplexer.Connect(_connectionString);
				}
				catch (RedisConnectionException e)
				{
					// Log a friendly message so we can easily see the issue
					_errorHandler.CaptureException(new Exception("Redis Cache is Unavailable", e));
				}

				return _connection;
			}
		}

		public void Dispose()
		{
			if (_connection != null)
				_connection.Dispose();
		}
	}
}
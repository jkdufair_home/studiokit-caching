﻿using Enyim.Caching;
using Enyim.Caching.Configuration;
using Enyim.Caching.Memcached;
using StudioKit.Caching.Clients.Base;
using StudioKit.CentralStudioKit.Interfaces;
using StudioKit.ErrorHandling.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace StudioKit.Caching.Clients
{
	public class MemcacheClient : ClientBase, ICacheClient
	{
		private static MemcachedClient _client;
		private readonly IStudioKitConfig _studioKitConfig;
		private readonly IErrorHandler _errorHandler;

		public MemcacheClient(IStudioKitConfig studioKitConfig, IErrorHandler errorHandler)
		{
			_errorHandler = errorHandler;
			_studioKitConfig = studioKitConfig;
			Prefix = _studioKitConfig.ApplicationName + "_";
		}

		private MemcachedClient Client
		{
			get
			{
				if (_client != null) return _client;
				var config = new MemcachedClientConfiguration();
				foreach (var server in _studioKitConfig.MemcachedServers)
				{
					config.AddServer(server.Address.ToString(), server.Port);
				}
				config.Transcoder = new DataContractTranscoder();
				_client = new MemcachedClient(config);
				return _client;
			}
		}

		/// <summary>
		///     A list of all the keys that are in Memcached.
		/// </summary>
		private List<string> CacheKeys
		{
			get { return (List<string>)Get(CacheKeysKey) ?? new List<string>(); }
		}

		#region ICacheClient Members

		public object Get(string key)
		{
			try
			{
				return Client.Get(Prefix + key);
			}
			catch (Exception ex)
			{
				_errorHandler.CaptureException(ex);
				return null;
			}
		}

		public T Get<T>(string key)
		{
			try
			{
				return (T)Client.Get(Prefix + key);
			}
			catch (Exception ex)
			{
				_errorHandler.CaptureException(ex);
				return default(T);
			}
		}

		public IEnumerable<KeyValuePair<string, object>> GetMany(IEnumerable<string> keys)
		{
			var keyArray = keys as string[] ?? keys.ToArray();
			var prefixedKeys = new List<string>(keyArray.Count());
			prefixedKeys.AddRange(keyArray.Select(key => Prefix + key));
			return Client.Get(prefixedKeys);
		}

		public bool Put(string key, object value)
		{
			var ret = Client.Store(StoreMode.Set, Prefix + key, value);
			var cacheKeys = CacheKeys;
			if (ret && !cacheKeys.Contains(key))
			{
				cacheKeys.Add(key);
				Put(CacheKeysKey, cacheKeys);
			}
			return true;
		}

		public bool Put(string key, object value, TimeSpan validFor)
		{
			var ret = Client.Store(StoreMode.Set, Prefix + key, value, validFor);
			var cacheKeys = CacheKeys;
			if (ret && !cacheKeys.Contains(key))
			{
				cacheKeys.Add(key);
				Put(CacheKeysKey, cacheKeys);
			}
			return true;
		}

		public long Increment(string key, long delta, long initialValue)
		{
			return Convert.ToInt64(Client.Increment(Prefix + key, (ulong)initialValue, (ulong)delta));
		}

		public long Decrement(string key, long delta, long initialValue)
		{
			return Convert.ToInt64(Client.Decrement(Prefix + key, (ulong)initialValue, (ulong)delta));
		}

		public bool Remove(string key)
		{
			var ret = Client.Remove(Prefix + key);
			if (ret)
			{
				var cacheKeys = CacheKeys;
				cacheKeys.Remove(key);
				Put(CacheKeysKey, cacheKeys);
			}
			return true;
		}

		public void PurgeByPrefix(string prefix)
		{
			ThisPrefix = prefix;
			var purgeList = CacheKeys.FindAll(HasPrefix);
			foreach (var key in purgeList)
			{
				Remove(key);
			}
		}

		public void PurgeByPrefixList(IEnumerable<string> prefixEnumerable)
		{
			var purgeList = new List<string>();
			foreach (
				var key in
					prefixEnumerable.SelectMany(
						prefix => CacheKeys.Where(key => key.StartsWith(prefix) && !purgeList.Contains(key))))
			{
				purgeList.Add(key);
			}

			foreach (var key in purgeList)
			{
				Remove(key);
			}
		}

		public void PurgeByContains(string contains)
		{
			ThisContains = contains;
			var purgeList = CacheKeys.FindAll(HasContains);
			foreach (var key in purgeList)
			{
				Remove(key);
			}
		}

		public void PurgeByContainsList(IEnumerable<string> contiansEnumerable)
		{
			var purgeList = new List<string>();
			foreach (
				var key in
					contiansEnumerable.SelectMany(
						contains => CacheKeys.Where(key => key.Contains(contains) && !purgeList.Contains(key))))
			{
				purgeList.Add(key);
			}

			foreach (var key in purgeList)
			{
				Remove(key);
			}
		}

		public void Flush()
		{
			Client.FlushAll();
		}

		#endregion ICacheClient Members
	}
}
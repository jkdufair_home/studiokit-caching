﻿namespace StudioKit.Caching.Clients.Base
{
	public class ClientBase
	{
		protected const string CacheKeysKey = "CACHEKEYS";
		protected static string Prefix = "";
		protected static string ThisPrefix;
		protected static string ThisContains;

		/// <summary>
		///     Delegate method for CacheKeys list. Case sensitive detection of prefixes.
		/// </summary>
		protected static bool HasPrefix(string str)
		{
			return str.StartsWith(ThisPrefix);
		}

		/// <summary>
		///     Delegate method for CacheKeys list. Case sensitive detection of a string within a string.
		/// </summary>
		protected static bool HasContains(string str)
		{
			return str.Contains(ThisContains);
		}
	}
}
﻿using Microsoft.ApplicationServer.Caching;
using StudioKit.Caching.Clients.Base;
using StudioKit.CentralStudioKit.Interfaces;
using StudioKit.ErrorHandling.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace StudioKit.Caching.Clients
{
	public class AzureClient : ClientBase, ICacheClient
	{
		private readonly IStudioKitServerEnvironment _serverEnvironment;
		private readonly IErrorHandler _errorHandler;

		public AzureClient(IStudioKitServerEnvironment serverEnvironment, IErrorHandler errorHandler)
		{
			_errorHandler = errorHandler;
			_serverEnvironment = serverEnvironment;
		}

		private DataCache _client;

		private DataCache Client
		{
			get
			{
				if (_client != null) return _client;
				var config = new DataCacheFactoryConfiguration
				{
					AutoDiscoverProperty =
						new DataCacheAutoDiscoverProperty(true, _serverEnvironment.AzureCacheUrl),
					SecurityProperties = new DataCacheSecurity(_serverEnvironment.AzureCacheKey, false)
				};
				var factory = new DataCacheFactory(config);
				_client = factory.GetDefaultCache();
				return _client;
			}
		}

		/// <summary>
		///     A list of all the keys that are in the Azure cache
		/// </summary>
		private List<string> CacheKeys
		{
			get { return (List<string>)Get(CacheKeysKey) ?? new List<string>(); }
		}

		#region ICacheClient Members

		public object Get(string key)
		{
			try
			{
				return Client.Get(Prefix + key);
			}
			catch (Exception ex)
			{
				_errorHandler.CaptureException(ex);
				return null;
			}
		}

		public T Get<T>(string key)
		{
			try
			{
				return (T)Client.Get(Prefix + key);
			}
			catch (Exception ex)
			{
				_errorHandler.CaptureException(ex);
				return default(T);
			}
		}

		public IEnumerable<KeyValuePair<string, object>> GetMany(IEnumerable<string> keys)
		{
			var keyArray = keys as string[] ?? keys.ToArray();
			var prefixedKeys = new List<string>(keyArray.Count());
			prefixedKeys.AddRange(keyArray.Select(key => Prefix + key));
			return Client.BulkGet(prefixedKeys);
		}

		public bool Put(string key, object value)
		{
			var ret = Client.Put(Prefix + key, value);
			var cacheKeys = CacheKeys;
			if (ret != null && !cacheKeys.Contains(key))
			{
				cacheKeys.Add(key);
				Put(CacheKeysKey, cacheKeys);
			}
			return true;
		}

		public bool Put(string key, object value, TimeSpan validFor)
		{
			var ret = Client.Put(Prefix + key, value, validFor);
			var cacheKeys = CacheKeys;
			if (ret != null && !cacheKeys.Contains(key))
			{
				cacheKeys.Add(key);
				Put(CacheKeysKey, cacheKeys);
			}
			return true;
		}

		public long Increment(string key, long delta, long initialValue)
		{
			return Client.Increment(Prefix + key, initialValue, delta);
		}

		public long Decrement(string key, long delta, long initialValue)
		{
			return Client.Decrement(Prefix + key, initialValue, delta);
		}

		public bool Remove(string key)
		{
			var ret = Client.Remove(Prefix + key);
			if (ret)
			{
				var cacheKeys = CacheKeys;
				cacheKeys.Remove(key);
				Put(CacheKeysKey, cacheKeys);
			}
			return true;
		}

		public void PurgeByPrefix(string prefix)
		{
			ThisPrefix = prefix;
			var purgeList = CacheKeys.FindAll(HasPrefix);
			foreach (var key in purgeList)
			{
				Remove(key);
			}
		}

		public void PurgeByPrefixList(IEnumerable<string> prefixEnumerable)
		{
			var purgeList = new List<string>();
			foreach (
				var key in
					prefixEnumerable.SelectMany(
						prefix => CacheKeys.Where(key => key.StartsWith(prefix) && !purgeList.Contains(key))))
			{
				purgeList.Add(key);
			}

			foreach (var key in purgeList)
			{
				Remove(key);
			}
		}

		public void PurgeByContains(string contains)
		{
			ThisContains = contains;
			var purgeList = CacheKeys.FindAll(HasContains);
			foreach (var key in purgeList)
			{
				Remove(key);
			}
		}

		public void PurgeByContainsList(IEnumerable<string> contiansEnumerable)
		{
			var purgeList = new List<string>();
			foreach (
				var key in
					contiansEnumerable.SelectMany(
						contains => CacheKeys.Where(key => key.Contains(contains) && !purgeList.Contains(key))))
			{
				purgeList.Add(key);
			}
			foreach (var key in purgeList)
			{
				Remove(key);
			}
		}

		public void Flush()
		{
			Client.Clear();
		}

		#endregion ICacheClient Members
	}
}
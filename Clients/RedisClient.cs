﻿using StackExchange.Redis;
using StudioKit.Caching.Clients.Base;
using StudioKit.CentralStudioKit.Interfaces;
using StudioKit.ErrorHandling.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;

namespace StudioKit.Caching.Clients
{
	public class RedisClient : ClientBase, IAsyncCacheClient
	{
		private readonly RedisConnectionFactory _redisConnectionFactory;
		private readonly IErrorHandler _errorHandler;
		private IDatabase _cacheDatabase;

		public RedisClient(RedisConnectionFactory redisConnectionFactory, IErrorHandler errorHandler)
		{
			_errorHandler = errorHandler;
			_redisConnectionFactory = redisConnectionFactory;
		}

		public ConnectionMultiplexer Connection
		{
			get { return _redisConnectionFactory.Connection; }
		}

		public IDatabase CacheDatabase
		{
			get
			{
				if (_cacheDatabase != null)
					return _cacheDatabase;

				// try to connect
				if (Connection == null)
					return null;

				// try to get database
				_cacheDatabase = Connection.GetDatabase();

				return _cacheDatabase;
			}
		}

		/// <summary>
		///     A list of all the keys that are in the Azure cache
		/// </summary>
		private List<string> CacheKeys
		{
			get { return (List<string>)Get(CacheKeysKey) ?? new List<string>(); }
		}

		#region ICacheClient Members

		public object Get(string key)
		{
			if (CacheDatabase == null) return null;

			try
			{
				return Deserialize<object>(CacheDatabase.StringGet(Prefix + key));
			}
			catch (Exception ex)
			{
				_errorHandler.CaptureException(ex);
				return null;
			}
		}

		public T Get<T>(string key)
		{
			if (CacheDatabase == null) return default(T);

			return Deserialize<T>(CacheDatabase.StringGet(Prefix + key));
		}

		public IEnumerable<KeyValuePair<string, object>> GetMany(IEnumerable<string> keys)
		{
			if (CacheDatabase == null) return null;

			var enumerable = keys as IList<string> ?? keys.ToList();
			var prefixedKeys = new List<string>(enumerable.Count());
			prefixedKeys.AddRange(enumerable.Select(key => Prefix + key));
			var redisKeys = prefixedKeys.Select(key => (RedisKey)key).ToList();
			var values = CacheDatabase.StringGet(redisKeys.ToArray());
			return enumerable.Zip(values,
				(s1, s2) => new KeyValuePair<string, object>(s1, s2.IsNullOrEmpty ? null : Deserialize<object>(s2)));
		}

		public bool Put(string key, object value)
		{
			if (CacheDatabase == null) return false;

			var ret = CacheDatabase.StringSet(Prefix + key, Serialize(value));
			var cacheKeys = CacheKeys;
			if (ret && !cacheKeys.Contains(key))
			{
				cacheKeys.Add(key);
				Put(CacheKeysKey, cacheKeys);
			}
			return true;
		}

		public bool Put(string key, object value, TimeSpan validFor)
		{
			if (CacheDatabase == null) return false;

			var ret = CacheDatabase.StringSet(Prefix + key, Serialize(value), validFor);
			var cacheKeys = CacheKeys;
			if (ret && !cacheKeys.Contains(key))
			{
				cacheKeys.Add(key);
				Put(CacheKeysKey, cacheKeys);
			}
			return true;
		}

		public long Increment(string key, long delta, long initialValue)
		{
			if (CacheDatabase == null) return 0;

			var exists = CacheDatabase.KeyExists(key);
			if (!exists)
				Put(Prefix + key, initialValue);
			return CacheDatabase.StringIncrement(Prefix + key, delta);
		}

		public long Decrement(string key, long delta, long initialValue)
		{
			if (CacheDatabase == null) return 0;

			var exists = CacheDatabase.KeyExists(key);
			if (!exists)
				Put(Prefix + key, initialValue);
			return CacheDatabase.StringDecrement(Prefix + key, delta);
		}

		public bool Remove(string key)
		{
			if (CacheDatabase == null) return false;

			var ret = CacheDatabase.KeyDelete(Prefix + key);
			if (ret)
			{
				var cacheKeys = CacheKeys;
				cacheKeys.Remove(key);
				Put(CacheKeysKey, cacheKeys);
			}
			return true;
		}

		public void PurgeByPrefix(string prefix)
		{
			ThisPrefix = prefix;
			var purgeList = CacheKeys.FindAll(HasPrefix);
			foreach (var key in purgeList)
			{
				Remove(key);
			}
		}

		public void PurgeByPrefixList(IEnumerable<string> prefixEnumerable)
		{
			var purgeList = new List<string>();
			foreach (
				var key in
					prefixEnumerable.SelectMany(
						prefix => CacheKeys.Where(key => key.StartsWith(prefix) && !purgeList.Contains(key))))
			{
				purgeList.Add(key);
			}

			foreach (var key in purgeList)
			{
				Remove(key);
			}
		}

		public void PurgeByContains(string contains)
		{
			ThisContains = contains;
			var purgeList = CacheKeys.FindAll(HasContains);
			foreach (var key in purgeList)
			{
				Remove(key);
			}
		}

		public void PurgeByContainsList(IEnumerable<string> contiansEnumerable)
		{
			var purgeList = new List<string>();
			foreach (
				var key in
					contiansEnumerable.SelectMany(
						contains => CacheKeys.Where(key => key.Contains(contains) && !purgeList.Contains(key))))
			{
				purgeList.Add(key);
			}
			foreach (var key in purgeList)
			{
				Remove(key);
			}
		}

		/// <summary>
		///     Flushes the Redis Cache database. allowAdmin must be set to true in the configuration options that are used to
		///     create
		///     the connection, or this will throw an error.
		/// </summary>
		public void Flush()
		{
			if (_redisConnectionFactory.Connection == null) return;
			_redisConnectionFactory.Connection.GetServer(_redisConnectionFactory.Connection.GetEndPoints().First()).FlushDatabase();
		}

		public async Task<object> GetAsync(string key)
		{
			if (CacheDatabase == null) return null;

			try
			{
				return Deserialize<object>(await CacheDatabase.StringGetAsync(Prefix + key));
			}
			catch (Exception ex)
			{
				_errorHandler.CaptureException(ex);
				return null;
			}
		}

		public async Task<T> GetAsync<T>(string key)
		{
			if (CacheDatabase == null) return default(T);

			return Deserialize<T>(await CacheDatabase.StringGetAsync(Prefix + key));
		}

		public async Task<IEnumerable<KeyValuePair<string, object>>> GetManyAsync(IEnumerable<string> keys)
		{
			if (CacheDatabase == null) return null;

			var enumerable = keys as IList<string> ?? keys.ToList();
			var prefixedKeys = new List<string>(enumerable.Count());
			prefixedKeys.AddRange(enumerable.Select(key => Prefix + key));
			var redisKeys = prefixedKeys.Select(key => (RedisKey)key).ToList();
			var values = await CacheDatabase.StringGetAsync(redisKeys.ToArray());
			return enumerable.Zip(values,
				(s1, s2) => new KeyValuePair<string, object>(s1, s2.IsNullOrEmpty ? null : Deserialize<object>(s2)));
		}

		public async Task<bool> PutAsync(string key, object value)
		{
			if (CacheDatabase == null) return false;

			var ret = await CacheDatabase.StringSetAsync(Prefix + key, Serialize(value));
			var cacheKeys = CacheKeys;
			if (ret && !cacheKeys.Contains(key))
			{
				cacheKeys.Add(key);
				await PutAsync(CacheKeysKey, cacheKeys);
			}
			return true;
		}

		public async Task<bool> PutAsync(string key, object value, TimeSpan validFor)
		{
			if (CacheDatabase == null) return false;

			var ret = await CacheDatabase.StringSetAsync(Prefix + key, Serialize(value), validFor);
			var cacheKeys = CacheKeys;
			if (ret && !cacheKeys.Contains(key))
			{
				cacheKeys.Add(key);
				await PutAsync(CacheKeysKey, cacheKeys);
			}
			return true;
		}

		public async Task<long> IncrementAsync(string key, long delta, long initialValue)
		{
			if (CacheDatabase == null) return 0;

			var exists = await CacheDatabase.KeyExistsAsync(key);
			if (!exists)
				await PutAsync(Prefix + key, initialValue);
			return await CacheDatabase.StringIncrementAsync(Prefix + key, delta);
		}

		public async Task<long> DecrementAsync(string key, long delta, long initialValue)
		{
			if (CacheDatabase == null) return 0;

			var exists = await CacheDatabase.KeyExistsAsync(key);
			if (!exists)
				await PutAsync(Prefix + key, initialValue);
			return await CacheDatabase.StringDecrementAsync(Prefix + key, delta);
		}

		public async Task<bool> RemoveAsync(string key)
		{
			if (CacheDatabase == null) return false;

			var ret = await CacheDatabase.KeyDeleteAsync(Prefix + key);
			if (ret)
			{
				var cacheKeys = CacheKeys;
				cacheKeys.Remove(key);
				await PutAsync(CacheKeysKey, cacheKeys);
			}
			return true;
		}

		public async Task PurgeByPrefixAsync(string prefix)
		{
			ThisPrefix = prefix;
			var purgeList = CacheKeys.FindAll(HasPrefix);
			foreach (var key in purgeList)
			{
				await RemoveAsync(key);
			}
		}

		public async Task PurgeByPrefixListAsync(IEnumerable<string> prefixEnumerable)
		{
			var purgeList = new List<string>();
			foreach (
				var key in
					prefixEnumerable.SelectMany(
						prefix => CacheKeys.Where(key => key.StartsWith(prefix) && !purgeList.Contains(key))))
			{
				purgeList.Add(key);
			}

			foreach (var key in purgeList)
			{
				await RemoveAsync(key);
			}
		}

		public async Task PurgeByContainsAsync(string contains)
		{
			ThisContains = contains;
			var purgeList = CacheKeys.FindAll(HasContains);
			foreach (var key in purgeList)
			{
				await RemoveAsync(key);
			}
		}

		public async Task PurgeByContainsListAsync(IEnumerable<string> contiansEnumerable)
		{
			var purgeList = new List<string>();
			foreach (
				var key in
					contiansEnumerable.SelectMany(
						contains => CacheKeys.Where(key => key.Contains(contains) && !purgeList.Contains(key))))
			{
				purgeList.Add(key);
			}
			foreach (var key in purgeList)
			{
				await RemoveAsync(key);
			}
		}

		public async Task FlushAsync()
		{
			if (_redisConnectionFactory.Connection == null) return;
			await _redisConnectionFactory.Connection.GetServer(_redisConnectionFactory.Connection.GetEndPoints().First()).FlushDatabaseAsync();
		}

		#endregion ICacheClient Members

		#region Helpers

		private static byte[] Serialize(object o)
		{
			if (o == null)
			{
				return null;
			}

			var binaryFormatter = new BinaryFormatter();
			using (var memoryStream = new MemoryStream())
			{
				binaryFormatter.Serialize(memoryStream, o);
				var objectDataAsStream = memoryStream.ToArray();
				return objectDataAsStream;
			}
		}

		private static T Deserialize<T>(byte[] stream)
		{
			if (stream == null)
			{
				return default(T);
			}

			var binaryFormatter = new BinaryFormatter();
			using (var memoryStream = new MemoryStream(stream))
			{
				var result = (T)binaryFormatter.Deserialize(memoryStream);
				return result;
			}
		}

		#endregion Helpers
	}
}
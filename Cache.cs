using StudioKit.CentralStudioKit.Interfaces;
using StudioKit.ErrorHandling.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace StudioKit.Caching
{
	public class Cache
	{
		protected readonly ICacheClient CacheClient;
		protected readonly IErrorHandler ErrorHandler;

		public Cache(ICacheClient cacheClient, IErrorHandler errorHandler)
		{
			CacheClient = cacheClient;
			ErrorHandler = errorHandler;
		}

		/// <summary>
		///     Store data to cache
		/// </summary>
		public async Task Store(string key, object data)
		{
			await Task.Run(() =>
			{
				try
				{
					CacheClient.Put(key, data);
				}
				catch (Exception exception)
				{
					ErrorHandler.CaptureException(exception);
				}
			});
		}

		/// <summary>
		///     Store data to cache using the SET store mode with a set time-to-live
		/// </summary>
		public async Task Store(string key, object data, TimeSpan validFor)
		{
			await Task.Run(() =>
			{
				try
				{
					CacheClient.Put(key, data, validFor);
				}
				catch (Exception exception)
				{
					ErrorHandler.CaptureException(exception);
				}
			});
		}

		/// <summary>
		///     Fetch an object from cache by key.
		///     You should **NOT** use this for retrieving LINQ to SQL objects!
		/// </summary>
		public object Get(string key)
		{
			try
			{
				return CacheClient.Get(key);
			}
			catch (Exception exception)
			{
				ErrorHandler.CaptureException(exception);
				return null;
			}
		}

		/// <summary>
		///     Fetch a batch of keys/values from cache by key list
		/// </summary>
		public IEnumerable<KeyValuePair<string, object>> GetMany(List<string> keys)
		{
			try
			{
				return CacheClient.GetMany(keys);
			}
			catch (Exception exception)
			{
				ErrorHandler.CaptureException(exception);
				return null;
			}
		}

		/// <summary>
		///     Removes an object from cache by key
		/// </summary>
		public async Task Remove(string key)
		{
			await Task.Run(() =>
			{
				try
				{
					CacheClient.Remove(key);
				}
				catch (Exception exception)
				{
					ErrorHandler.CaptureException(exception);
				}
			});
		}

		public async Task PurgeByPrefix(string prefix)
		{
			await Task.Run(() =>
			{
				try
				{
					CacheClient.PurgeByPrefix(prefix);
				}
				catch (Exception exception)
				{
					ErrorHandler.CaptureException(exception);
				}
			});
		}

		public async Task PurgeByPrefixList(List<string> prefixList)
		{
			await Task.Run(() =>
			{
				try
				{
					CacheClient.PurgeByPrefixList(prefixList);
				}
				catch (Exception exception)
				{
					ErrorHandler.CaptureException(exception);
				}
			});
		}

		public async Task PurgeByContains(string contains)
		{
			await Task.Run(() =>
			{
				try
				{
					CacheClient.PurgeByContains(contains);
				}
				catch (Exception exception)
				{
					ErrorHandler.CaptureException(exception);
				}
			});
		}

		public async Task PurgeByContainsList(List<string> containsList)
		{
			await Task.Run(() =>
			{
				try
				{
					CacheClient.PurgeByContainsList(containsList);
				}
				catch (Exception exception)
				{
					ErrorHandler.CaptureException(exception);
				}
			});
		}

		/// <summary>
		///     Clear cache for the ENTIRE cached system and purge all cache keys
		/// </summary>
		public async Task Flush()
		{
			await Task.Run(() =>
			{
				try
				{
					CacheClient.Flush();
				}
				catch (Exception exception)
				{
					ErrorHandler.CaptureException(exception);
				}
			});
		}
	}
}